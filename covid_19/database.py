from datetime import datetime, timedelta
import io
import json
import urllib.request
from pathlib import Path

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
import pandas as pd

base_dir = Path(__file__).parent
sqlite_files = list(base_dir.glob('*.sqlite3'))
if len(sqlite_files) > 1:
    raise Exception(f'Only one sqlite database is designed to be located in {base_dir}')
if sqlite_files:
    db_file_path = sqlite_files[0]
else:
    db_file_path = base_dir / f'covid-19_{datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S")}Z.sqlite3'
engine = create_engine(f'sqlite:///{db_file_path}', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def regenerate_population_csv():
    with urllib.request.urlopen("https://population.un.org/wpp/Download/Files/1_Indicators%20(Standard)/CSV_FILES/"
                                "WPP2019_TotalPopulationBySex.csv") as population_url:
        population_df = pd.read_csv(io.StringIO(population_url.read().decode('UTF-8')))
    population_df = population_df[population_df['Time'] == 2020]
    population_df = population_df.drop(['LocID', 'VarID', 'Variant', 'Time', 'MidPeriod'], axis='columns')
    population_df = population_df.drop_duplicates()
    population_df = population_df.set_index('Location')
    population_df.to_csv('covid_19/data/population.csv')


def init_db(grade='prod'):
    print(f'Initializing database at location, {db_file_path} ...')
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    from covid_19.models import Country, EventDate, Event, FuzzyCountryLookup
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)

    # Database is generated from source files

    with urllib.request.urlopen("https://pomber.github.io/covid19/timeseries.json") as covid19_timeseries_url:
        covid19_timeseries_data = json.load(covid19_timeseries_url)

    population_df = pd.read_csv('covid_19/data/population.csv').set_index('Location')

    start_date_string = '01/22/2020'  # Date data starts
    start_date = datetime.strptime(start_date_string, "%m/%d/%Y").date()
    end_date = datetime.utcnow().date() + timedelta(days=1)  # One day from now to ensure we are covered

    for idx, n in enumerate(range((end_date - start_date).days + 1)):
        db_session.add(EventDate(id=idx, day=start_date + timedelta(days=n)))

    for country_id, (country, events) in enumerate(covid19_timeseries_data.items()):
        is_confirmed = False
        is_deaths = False
        is_recovered = False
        day_0_confirmed = None
        day_0_deaths = None
        day_0_recovered = None
        for event_id, event in enumerate(events):
            date_day = datetime.strptime(event['date'], '%Y-%m-%d').date()
            # TODO: Assert that event id corresponds to date id
            # assert event_id == date_day
            confirmed = event['confirmed']
            deaths = event['deaths']
            recovered = event['recovered']
            if not is_confirmed and confirmed != 0:
                day_0_confirmed = event_id
                is_confirmed = True
            if not is_deaths and deaths != 0:
                day_0_deaths = event_id
                is_deaths = True
            if not is_recovered and recovered != 0:
                day_0_recovered = event_id
                is_recovered = True
            db_session.add(
                Event(country_id=country_id, event_date_id=event_id, confirmed=confirmed, death=deaths, recovered=recovered)
            )
        try:
            country_stats = population_df.loc[country]
        except KeyError:
            alt_names = {
                'Bolivia': 'Bolivia (Plurinational State of)',
                'Brunei': 'Brunei Darussalam',
                'Congo (Brazzaville)': 'Congo',
                'Congo (Kinshasa)': 'Congo',
                'Cote d\'Ivoire': 'Côte d\'Ivoire',
                'Diamond Princess': None,
                'Iran': 'Iran (Islamic Republic of)',
                'Korea, South': 'Republic of Korea',
                'Moldova': 'Republic of Moldova',
                'MS Zaandam': None,
                'Russia': 'Russian Federation',
                'Taiwan*': 'China, Taiwan Province of China',
                'Tanzania': 'United Republic of Tanzania',
                'US': 'United States of America',
                'Venezuela': 'Venezuela (Bolivarian Republic of)',
                'Vietnam': 'Viet Nam',
                'Syria': 'Syrian Arab Republic',
                'Laos': 'Lao People\'s Democratic Republic',
                'West Bank and Gaza': None,
                'Kosovo': None,
                'Burma': 'Myanmar',
            }
            try:
                alt_name = alt_names[country]
            except KeyError:
                from fuzzywuzzy import process
                # TODO: Check generated database to see if there are Fuzzy matches that need to be explicitly handled
                choices = population_df.index.values.tolist()
                alt_name, score = process.extractOne(country, choices)
                if not grade == 'prod':
                    db_session.add(FuzzyCountryLookup(country_id=country_id, alt_name=alt_name, score=score))
            if alt_name:
                country_stats = population_df.loc[alt_name]
            else:
                country_stats = {
                    'PopMale': None,
                    'PopFemale': None,
                    'PopTotal': None,
                    'PopDensity': None
                }
        population_male = round(country_stats['PopMale'] * 1000) if country_stats['PopMale'] else None
        population_female = round(country_stats['PopFemale'] * 1000) if country_stats['PopFemale'] else None
        population_total = round(country_stats['PopTotal'] * 1000) if country_stats['PopTotal'] else None
        db_session.add(
            Country(
                id=country_id,
                name=country,
                day_0_confirmed=day_0_confirmed,
                day_0_deaths=day_0_deaths,
                day_0_recovered=day_0_recovered,
                population_male=population_male,
                population_female=population_female,
                population_total=population_total,
                population_density=country_stats['PopDensity']
            )
        )
    if grade == 'prod':
        FuzzyCountryLookup.__table__.drop(engine)
    db_session.commit()
