from covid_19.models import Country as CountryModel
from covid_19.models import EventDate as EventDateModel
from covid_19.models import Event as EventModel

from graphene import ObjectType, relay, Schema
from graphene_sqlalchemy import SQLAlchemyConnectionField, SQLAlchemyObjectType


class CountryNode(SQLAlchemyObjectType):
    class Meta:
        model = CountryModel
        interfaces = (relay.Node, )


class CountryConnection(relay.Connection):
    class Meta:
        node = CountryNode


class EventDateNode(SQLAlchemyObjectType):
    class Meta:
        model = EventDateModel
        interfaces = (relay.Node, )


class EventDateConnection(relay.Connection):
    class Meta:
        node = EventDateNode


class EventNode(SQLAlchemyObjectType):
    class Meta:
        model = EventModel
        interfaces = (relay.Node, )


class EventConnection(relay.Connection):
    class Meta:
        node = EventNode


class Query(ObjectType):
    node = relay.Node.Field()
    all_event_dates = SQLAlchemyConnectionField(EventDateConnection)
    all_events = SQLAlchemyConnectionField(EventConnection)
    all_countries = SQLAlchemyConnectionField(CountryConnection, sort=None)


schema = Schema(query=Query)
