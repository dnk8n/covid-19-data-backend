from covid_19.database import Base
from sqlalchemy import Column, Date, ForeignKey, Integer, Float, String, PrimaryKeyConstraint


class Country(Base):
    __tablename__ = 'country'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    day_0_confirmed = Column(Integer, ForeignKey('event_date.id'))
    day_0_recovered = Column(Integer, ForeignKey('event_date.id'))
    day_0_deaths = Column(Integer, ForeignKey('event_date.id'))
    population_male = Column(Integer)
    population_female = Column(Integer)
    population_total = Column(Integer)
    population_density = Column(Float)


class EventDate(Base):
    __tablename__ = 'event_date'
    id = Column(Integer, primary_key=True)
    day = Column(Date, unique=True)


class Event(Base):
    __tablename__ = 'event'
    country_id = Column(Integer, ForeignKey('country.id'))
    event_date_id = Column(Integer, ForeignKey('event_date.id'))
    confirmed = Column(Integer)
    death = Column(Integer)
    recovered = Column(Integer)
    __table_args__ = (PrimaryKeyConstraint('country_id', 'event_date_id'),)


class FuzzyCountryLookup(Base):
    __tablename__ = 'fuzzy_country_lookup'
    id = Column(Integer, primary_key=True)
    country_id = Column(Integer, ForeignKey('country.id'))
    alt_name = Column(String)
    score = Column(Integer)
