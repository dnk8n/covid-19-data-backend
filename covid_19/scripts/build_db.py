#!/usr/bin/env python

from sys import argv

from covid_19.database import init_db

if __name__ == '__main__':
    try:
        grade = argv[1]
    except IndexError:
        init_db()
    else:
        if grade == 'prod' or grade == 'dev':
            init_db(grade)
        else:
            print(f'Invalid DB grade, "{grade}". Options are "dev" or "prod"')
