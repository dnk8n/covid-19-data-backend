#!/usr/bin/env python
from datetime import datetime

from covid_19.database import db_session
from covid_19.schema import schema

from flask import Flask
from flask_graphql import GraphQLView

app = Flask(__name__)
app.debug = False

example_query = """
{
  allCountries {
    edges {
      node {
        id
        name
      }
    }
  }
}
"""

app.add_url_rule(
    '/graphql/', view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True), strict_slashes=False
)


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


if __name__ == '__main__':
    app.run()
