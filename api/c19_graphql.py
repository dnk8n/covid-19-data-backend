#!/usr/bin/env python

from covid_19.app import app


if __name__ == '__main__':
    app.debug = True
    app.run()
