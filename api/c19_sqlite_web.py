#!/usr/bin/env python

from datetime import datetime

from flask import send_from_directory
from sqlite_web.sqlite_web import app, initialize_app

from covid_19.database import db_file_path

initialize_app(db_file_path.as_posix(), read_only=True)


@app.route('/download/db')
def download_db():
    download = send_from_directory(
        db_file_path.parent,
        db_file_path.name,
        as_attachment=True
    )
    return download


if __name__ == '__main__':
    app.debug = True
    app.run()
